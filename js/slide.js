/*Slider beneficios del leasing*/
const slider_3 = document.querySelector("#section3_slider");
let sliderSection_3 = document.querySelectorAll(".section3_slider_sub");
let sliderSectionLast_3 = sliderSection_3[sliderSection_3.length - 1];
let section3_dot_item = document.querySelectorAll(".section3_dot_item");

slider_3.insertAdjacentElement("afterbegin",sliderSectionLast_3);

function Next_3(){
    let sliderSectionFirst = document.querySelectorAll(".section3_slider_sub")[0];
    slider_3.style.marginLeft = "-200%";
    slider_3.style.transition = "all 1.5s";

    setTimeout(function(){
        slider_3.style.transition = "none";
        slider_3.insertAdjacentElement("beforeend", sliderSectionFirst);
        slider_3.style.marginLeft = "-100%";
    } , 1500) 
}




/*Slider proveedores*/ 
const slider = document.querySelector("#section8_slider");
let sliderSection = document.querySelectorAll(".section8_sub");
let sliderSectionLast = sliderSection[sliderSection.length - 1];

const btnleft = document.querySelector("#slider_left");
const btnright = document.querySelector("#slider_right");

slider.insertAdjacentElement("afterbegin",sliderSectionLast);

function Next(){
    let sliderSectionFirst = document.querySelectorAll(".section8_sub")[0];
    slider.style.marginLeft = "-200%";
    slider.style.transition = "all 1.5s";

    setTimeout(function(){
        slider.style.transition = "none";
        slider.insertAdjacentElement("beforeend", sliderSectionFirst);
        slider.style.marginLeft = "-100%";
    } , 1500) 
}

function Prev(){
    let sliderSection = document.querySelectorAll(".section8_sub");
    let sliderSectionLast = sliderSection[sliderSection.length - 1];
    slider.style.marginLeft = "0%";
    slider.style.transition = "all 1.5s";

    setTimeout(function(){
        slider.style.transition = "none";
        slider.insertAdjacentElement("afterbegin", sliderSectionLast);
        slider.style.marginLeft = "-100%";
    } , 1500) 
}

btnright.addEventListener("click", function(){
    Next();
})

btnleft.addEventListener("click", function(){
    Prev();
})


/*Slider marcas confianza*/ 
const slider_9 = document.querySelector("#section9_slider");
let sliderSection_9 = document.querySelectorAll(".section9_sub_sub");
let sliderSectionLast_9 = sliderSection_9[sliderSection_9.length - 1];

const btnleft_9 = document.querySelector("#slider_left_9");
const btnright_9 = document.querySelector("#slider_right_9");

slider_9.insertAdjacentElement("afterbegin",sliderSectionLast_9);

function Next_9(){
    let sliderSectionFirst = document.querySelectorAll(".section9_sub_sub")[0];
    slider_9.style.marginLeft = "-200%";
    slider_9.style.transition = "all 1.5s";

    setTimeout(function(){
        slider_9.style.transition = "none";
        slider_9.insertAdjacentElement("beforeend", sliderSectionFirst);
        slider_9.style.marginLeft = "-100%";
    } , 1500) 
}

function Prev_9(){
    let sliderSection = document.querySelectorAll(".section9_sub_sub");
    let sliderSectionLast = sliderSection[sliderSection.length - 1];
    slider_9.style.marginLeft = "0%";
    slider_9.style.transition = "all 1.5s";

    setTimeout(function(){
        slider_9.style.transition = "none";
        slider_9.insertAdjacentElement("afterbegin", sliderSectionLast);
        slider_9.style.marginLeft = "-100%";
    } , 1500) 
}

btnright_9.addEventListener("click", function(){
    Next_9();
})

btnleft_9.addEventListener("click", function(){
    Prev_9();
})


//Automatico
section3_dot_item[0].classList.add("section3_active");

function myFunction(x) {
    
    
    if (x.matches) {
        let i=1;
        let indicador = 0;
        setInterval(function() {
            Next();
            Next_9();
            Next_3();
            if(i==6){
                section3_dot_item[i-1].classList.remove("section3_active");
                section3_dot_item[0].classList.add("section3_active");
                i=1;
                indicador = 1;
            }
            if(i<6 && indicador == 0){
                section3_dot_item[i-1].classList.remove("section3_active");
                section3_dot_item[i].classList.add("section3_active");
                i++;
            }
            indicador = 0;
        }, 3500); 
    } else{
        //clearInterval(refreshInterval);
    }
}

var x = window.matchMedia("(max-width: 500px)");
var y = window.matchMedia("(min-width: 500px)");
myFunction(x) // Call listener function at run time
//x.addListener(myFunction);
x.addEventListener("change",myFunction);


