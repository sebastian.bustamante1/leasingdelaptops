let btnItems = document.querySelectorAll(".section12_item");

for(let i=0; i<btnItems.length;i++){

    btnItems[i].addEventListener("click", function(e){
        let btn = e.target;
        let indicador = 0;
        
        if(btn.className == "section12_btn_item" && indicador == 0){
            btn.classList.add("active")
            btn.children[0].classList.add("active");
            btn.parentElement.children[1].classList.add("active");
            indicador = 1;
        }
        if(btn.className == "flaticon-chevron-arrow-down" && indicador==0){
            btn.classList.add("active");
            btn.parentElement.classList.add("active");
            btn.parentElement.parentElement.children[1].classList.add("active")
            indicador = 1;
        }
        if(btn.className == "flaticon-chevron-arrow-down active" && indicador==0){
            btn.classList.remove("active");
            btn.parentElement.classList.remove("active");
            btn.parentElement.parentElement.children[1].classList.remove("active")
            indicador = 1;
        }
        if(btn.className == "section12_btn_item active" && indicador == 0){
            btn.classList.remove("active")
            btn.children[0].classList.remove("active");
            btn.parentElement.children[1].classList.remove("active");
            indicador = 1;
        }
    })
}